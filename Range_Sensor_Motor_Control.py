import RPi.GPIO as GPIO
import time

# set up The trigger and echo for the distance sensor
TRIG=32
ECHO=22

GPIO.cleanup()
# Set GPIO Mode to BOARD
GPIO.setmode(GPIO.BOARD)

# define pwma and pwmb
m1 = 7
m2 = 18

# set up GPIO pins
GPIO.setup(m1, GPIO.OUT) # Connected to PWMA
GPIO.setup(11, GPIO.OUT) # Connected to AIN2
GPIO.setup(12, GPIO.OUT) # Connected to AIN1
GPIO.setup(13, GPIO.OUT) # Connected to STBY
GPIO.setup(15, GPIO.OUT) # Connected to BIN1
GPIO.setup(16, GPIO.OUT) # Connected to BIN2
GPIO.setup(m2, GPIO.OUT) # Connected to PWMB


# set motor 1 and 2 speeds to low
m1s=GPIO.PWM(m1,1000)
m2s=GPIO.PWM(m2,1000)

# Initiate motor speed to low
m1s.start(50)
m2s.start(50)

# Drive the motor FORWARD
# Motor A:
GPIO.output(12, GPIO.HIGH) # Set AIN1
GPIO.output(11, GPIO.LOW) # Set AIN2
# Motor B:
GPIO.output(15, GPIO.LOW) # Set BIN1
GPIO.output(16, GPIO.HIGH) # Set BIN2

time.sleep(2)

# update motor speed
m1s.ChangeDutyCycle(30)
m2s.ChangeDutyCycle(30)

# # Set the motor speed
# # Motor A:
# GPIO.output(7, GPIO.HIGH) # Set PWMA
# # Motor B:
# GPIO.output(18, GPIO.HIGH) # Set PWMB

# Disable STBY (standby)
GPIO.output(13, GPIO.HIGH)

while True:
    # GPIO.setmode(GPIO.BCM)
    print("distance measurement in progress")
    GPIO.setup(TRIG,GPIO.OUT)
    GPIO.setup(ECHO,GPIO.IN)
    GPIO.output(TRIG,False)
    print("waiting for sensor to settle")
    time.sleep(0.1)
    GPIO.output(TRIG,True)
    time.sleep(0.00001)
    GPIO.output(TRIG,False)
    while GPIO.input(ECHO)==0:
        pulse_start=time.time()
    while GPIO.input(ECHO)==1:
        pulse_end=time.time()
    pulse_duration=pulse_end - pulse_start
    distance=pulse_duration * 17150
    distance=round(distance,2)
    int(distance)
    print("distance:",distance,"cm")
    time.sleep(0.1)

    if distance <= 45:
        # GPIO.setmode(GPIO.BOARD)
        # Wait 5 seconds
        # time.sleep(5)
        
        # update motor speed
        m1s.ChangeDutyCycle(75)
        m2s.ChangeDutyCycle(75)

        # Drive the motor BACKWARD
        # Motor A:
        GPIO.output(12, GPIO.LOW) # Set AIN1
        GPIO.output(11, GPIO.HIGH) # Set AIN2
        # Motor B:
        GPIO.output(15, GPIO.HIGH) # Set BIN1
        GPIO.output(16, GPIO.LOW) # Set BIN2

        # Set the motor speed
        # Motor A:
        GPIO.output(7, GPIO.HIGH) # Set PWMA
        # Motor B:
        GPIO.output(18, GPIO.HIGH) # Set PWMB

        # Disable STBY (standby)
        GPIO.output(13, GPIO.HIGH)

        # Wait 5 seconds
        time.sleep(3)

        # Reset all the GPIO pins by setting them to LOW
        GPIO.output(12, GPIO.LOW) # Set AIN1
        GPIO.output(11, GPIO.LOW) # Set AIN2
        GPIO.output(7, GPIO.LOW) # Set PWMA
        GPIO.output(13, GPIO.LOW) # Set STBY
        GPIO.output(15, GPIO.LOW) # Set BIN1
        GPIO.output(16, GPIO.LOW) # Set BIN2
        GPIO.output(18, GPIO.LOW) # Set PWMB