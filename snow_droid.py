"""snow_driod script which handles all functions of the Unit"""
import time
import RPi.GPIO as GPIO

# set up The trigger and echo for the ultrasonic sensor
TRIG = 32
ECHO = 22

# Set GPIO Mode to BOARD
GPIO.setmode(GPIO.BOARD)

# define pwma and pwmb
MOTOR1 = 7
MOTOR2 = 18

# set up GPIO pins
GPIO.setup(MOTOR1, GPIO.OUT)  # Connected to PWMA
GPIO.setup(11, GPIO.OUT)  # Connected to AIN2
GPIO.setup(12, GPIO.OUT)  # Connected to AIN1
GPIO.setup(13, GPIO.OUT)  # Connected to STBY
GPIO.setup(15, GPIO.OUT)  # Connected to BIN1
GPIO.setup(16, GPIO.OUT)  # Connected to BIN2
GPIO.setup(MOTOR2, GPIO.OUT)  # Connected to PWMB


def main():
    """main function"""
    drive_forward_high()
    time.sleep(2)
    turn_left()
    drive_forward_high()
    time.sleep(2)
    turn_left()
    drive_forward_high()
    time.sleep(2)
    turn_left()
    drive_forward_high()
    time.sleep(2)
    turn_left()


def turn_left():
    """Turn Snowdroid left"""
    # set motor 1 and 2 speeds
    motor1speed = GPIO.PWM(MOTOR1,1000)
    motor2speed = GPIO.PWM(MOTOR2,1000)

    # Initiate motor speed
    motor1speed.start(100)
    motor2speed.start(100)

    # Drive the motor FORWARD
    # Motor A:
    GPIO.output(12, GPIO.HIGH) # Set AIN1
    GPIO.output(11, GPIO.LOW) # Set AIN2
    # Motor B:
    GPIO.output(15, GPIO.LOW) # Set BIN1
    GPIO.output(16, GPIO.HIGH) # Set BIN2
    time.sleep(2)
    motor_shut_down()


def turn_right():
    """Turn Snowdroid right"""
    # set motor 1 and 2 speeds
    motor1speed = GPIO.PWM(MOTOR1,1000)
    motor2speed = GPIO.PWM(MOTOR2,1000)

    # Initiate motor speed
    motor1speed.start(100)
    motor2speed.start(100)

    # Drive the motor FORWARD
    # Motor A:
    GPIO.output(12, GPIO.LOW) # Set AIN1
    GPIO.output(11, GPIO.HIGH) # Set AIN2
    # Motor B:
    GPIO.output(15, GPIO.HIGH) # Set BIN1
    GPIO.output(16, GPIO.LOW) # Set BIN2
    time.sleep(2)
    motor_shut_down()

def drive_forward_high():
    """Drive Forward: HIGH"""
    # set motor 1 and 2 speeds
    motor1speed = GPIO.PWM(MOTOR1,4000)
    motor2speed = GPIO.PWM(MOTOR2,4000)

    # Initiate motor speed
    motor1speed.start(100)
    motor2speed.start(100)

    # Drive the motor FORWARD
    # Motor A:
    GPIO.output(12, GPIO.HIGH) # Set AIN1
    GPIO.output(11, GPIO.LOW) # Set AIN2
    # Motor B:
    GPIO.output(15, GPIO.HIGH) # Set BIN1
    GPIO.output(16, GPIO.LOW) # Set BIN2


def drive_forward_medium():
    """Drive Forward: MEDIUM"""
    # set motor 1 and 2 speed
    motor1speed = GPIO.PWM(MOTOR1,4000)
    motor2speed = GPIO.PWM(MOTOR2,4000)

    # Initiate motor speed
    motor1speed.start(75)
    motor2speed.start(75)

    # Drive the motor FORWARD
    # Motor A:
    GPIO.output(12, GPIO.HIGH) # Set AIN1
    GPIO.output(11, GPIO.LOW) # Set AIN2
    # Motor B:
    GPIO.output(15, GPIO.HIGH) # Set BIN1
    GPIO.output(16, GPIO.LOW) # Set BIN2


def drive_forward_low():
    """Drive Forward: LOW"""
    # set motor 1 and 2 speeds to low
    motor1speed = GPIO.PWM(MOTOR1,1000)
    motor2speed = GPIO.PWM(MOTOR2,1000)

    # Initiate motor speed to low
    motor1speed.start(50)
    motor2speed.start(50)

    # Drive the motor FORWARD
    # Motor A:
    GPIO.output(12, GPIO.HIGH) # Set AIN1
    GPIO.output(11, GPIO.LOW) # Set AIN2
    # Motor B:
    GPIO.output(15, GPIO.HIGH) # Set BIN1
    GPIO.output(16, GPIO.LOW) # Set BIN2


def drive_reverse_high():
    """Drive Reverse: HIGH"""
    # set motor 1 and 2 speed
    motor1speed = GPIO.PWM(MOTOR1,4000)
    motor2speed = GPIO.PWM(MOTOR2,4000)

    # Initiate motor speed
    motor1speed.start(100)
    motor2speed.start(100)

    # Drive the motor BACKWARD
    # Motor A:
    GPIO.output(12, GPIO.LOW) # Set AIN1
    GPIO.output(11, GPIO.HIGH) # Set AIN2
    # Motor B:
    GPIO.output(15, GPIO.LOW) # Set BIN1
    GPIO.output(16, GPIO.HIGH) # Set BIN2


def drive_reverse_medium():
    """Drive Reverse: MEDIUM"""
    # set motor 1 and 2 speed
    motor1speed = GPIO.PWM(MOTOR1,2000)
    motor2speed = GPIO.PWM(MOTOR2,2000)

    # Initiate motor speed
    motor1speed.start(75)
    motor2speed.start(75)

    # Drive the motor BACKWARD
    # Motor A:
    GPIO.output(12, GPIO.LOW) # Set AIN1
    GPIO.output(11, GPIO.HIGH) # Set AIN2
    # Motor B:
    GPIO.output(15, GPIO.LOW) # Set BIN1
    GPIO.output(16, GPIO.HIGH) # Set BIN2


def drive_reverse_low():
    """Drive Reverse: LOW"""
    # set motor 1 and 2 speed
    motor1speed = GPIO.PWM(MOTOR1,1000)
    motor2speed = GPIO.PWM(MOTOR2,1000)

    # Initiate motor speed
    motor1speed.start(50)
    motor2speed.start(50)

    # Drive the motor BACKWARD
    # Motor A:
    GPIO.output(12, GPIO.LOW) # Set AIN1
    GPIO.output(11, GPIO.HIGH) # Set AIN2
    # Motor B:
    GPIO.output(15, GPIO.LOW) # Set BIN1
    GPIO.output(16, GPIO.HIGH) # Set BIN2


def motor_shut_down():
    """Shut Down the Motors"""
    # Disable STBY (standby)
    GPIO.output(13, GPIO.HIGH)

    # Reset all the GPIO pins by setting them to LOW
    GPIO.output(12, GPIO.LOW) # Set AIN1
    GPIO.output(11, GPIO.LOW) # Set AIN2
    GPIO.output(7, GPIO.LOW) # Set PWMA
    GPIO.output(13, GPIO.LOW) # Set STBY
    GPIO.output(15, GPIO.LOW) # Set BIN1
    GPIO.output(16, GPIO.LOW) # Set BIN2
    GPIO.output(18, GPIO.LOW) # Set PWMB


def detect_objects_ultrasonic():
    """Measure distance from objects"""
    print("distance measurement in progress")
    GPIO.setup(TRIG,GPIO.OUT)
    GPIO.setup(ECHO,GPIO.IN)
    GPIO.output(TRIG,False)
    print("waiting for sensor to settle")
    time.sleep(0.1)
    GPIO.output(TRIG,True)
    time.sleep(0.00001)
    GPIO.output(TRIG,False)
    while GPIO.input(ECHO)==0:
        pulse_start=time.time()
    while GPIO.input(ECHO)==1:
        pulse_end=time.time()
    pulse_duration=pulse_end - pulse_start
    distance=pulse_duration * 17150
    distance=round(distance,2)
    int(distance)
    print("distance:",distance,"cm")
    time.sleep(0.1)

if __name__ == "__main__":
    main()
